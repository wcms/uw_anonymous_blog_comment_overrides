<?php

/**
 * @file
 * uw_anonymous_blog_comment_overrides.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_anonymous_blog_comment_overrides_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: captcha_points.
  $overrides["captcha_points.comment_node_uw_blog_form.captcha_type"] = 'reCAPTCHA';
  $overrides["captcha_points.comment_node_uw_blog_form.module"] = 'recaptcha';

  // Exported overrides for: variable.
  $overrides["variable.comment_anonymous_uw_blog.value"] = 1;
  $overrides["variable.comment_uw_blog.value"] = 2;

  return $overrides;
}
