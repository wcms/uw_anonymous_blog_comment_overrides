<?php

/**
 * @file
 * uw_anonymous_blog_comment_overrides.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_anonymous_blog_comment_overrides_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_captcha_default_points_alter().
 */
function uw_anonymous_blog_comment_overrides_captcha_default_points_alter(&$data) {
  if (isset($data['comment_node_uw_blog_form'])) {
    $data['comment_node_uw_blog_form']->captcha_type = 'reCAPTCHA'; /* WAS: '' */
    $data['comment_node_uw_blog_form']->module = 'recaptcha'; /* WAS: '' */
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_anonymous_blog_comment_overrides_strongarm_alter(&$data) {
  if (isset($data['comment_anonymous_uw_blog'])) {
    $data['comment_anonymous_uw_blog']->value = 1; /* WAS: 0 */
  }
  if (isset($data['comment_uw_blog'])) {
    $data['comment_uw_blog']->value = 2; /* WAS: 1 */
  }
}
