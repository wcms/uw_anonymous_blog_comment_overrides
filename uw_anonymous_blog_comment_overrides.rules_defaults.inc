<?php

/**
 * @file
 * uw_anonymous_blog_comment_overrides.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function uw_anonymous_blog_comment_overrides_default_rules_configuration() {
  $items = array();
  $items['rules_email_notification_to_site_managers_when_blog_comment_adde'] = entity_import('rules_config', '{ "rules_email_notification_to_site_managers_when_blog_comment_adde" : {
      "LABEL" : "Email notification to site managers when blog comment added",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "comment", "email" ],
      "REQUIRES" : [ "rules", "mimemail", "comment" ],
      "ON" : { "comment_insert--comment_node_uw_blog" : { "bundle" : "comment_node_uw_blog" } },
      "DO" : [
        { "mimemail_to_users_of_role" : {
            "key" : "comment_email_notice",
            "roles" : { "value" : { "6" : "6" } },
            "active" : 1,
            "from_name" : "[comment:author]",
            "from_mail" : "[comment:author:mail]",
            "subject" : "Comment added to the [site:name] site",
            "body" : "A new comment has been added to the [site:name] site that requires your review and approval.\\u003Cbr\\/\\u003E\\u003Cbr\\/\\u003E\\r\\n\\r\\n\\u003Cstrong\\u003EComment content:\\u003C\\/strong\\u003E\\u003Cbr\\/\\u003E\\r\\n[comment:body]\\r\\n\\u003Cbr\\/\\u003E\\u003Cbr\\/\\u003E\\r\\n\\u003Cstrong\\u003EComment link:\\u003C\\/strong\\u003E\\u003Cbr\\/\\u003E\\r\\n\\u003Ca href=\\u0022[comment:url]\\u0022\\u003E[comment:url]\\u003C\\/a\\u003E",
            "plaintext" : "A new comment has been added to the [site:name] site that requires your review and approval.\\r\\n\\r\\nComment content:\\r\\n[comment:body]\\r\\n\\r\\nComment url link: [comment:url]",
            "language_user" : 0,
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  return $items;
}
