<?php

/**
 * @file
 * uw_anonymous_blog_comment_overrides.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_anonymous_blog_comment_overrides_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'entity_translation_comment_filter_uw_blog';
  $strongarm->value = 0;
  $export['entity_translation_comment_filter_uw_blog'] = $strongarm;

  return $export;
}
